<?php

namespace App\Database\Types\Mysql;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use App\Database\Types\Type;

class MediumTextType extends Type
{
    const NAME = 'mediumtext';

    public function getSQLDeclaration(array $field, AbstractPlatform $platform)
    {
        return 'mediumtext';
    }
}

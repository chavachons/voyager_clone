<?php

namespace App\Database\Types\Mysql;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use App\Database\Types\Type;

class YearType extends Type
{
    const NAME = 'year';

    public function getSQLDeclaration(array $field, AbstractPlatform $platform)
    {
        return 'year';
    }
}

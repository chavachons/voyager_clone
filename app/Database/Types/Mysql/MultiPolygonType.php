<?php

namespace App\Database\Types\Mysql;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use App\Database\Types\Type;

class MultiPolygonType extends Type
{
    const NAME = 'multipolygon';

    public function getSQLDeclaration(array $field, AbstractPlatform $platform)
    {
        return 'multipolygon';
    }
}
